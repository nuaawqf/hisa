/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    cellDisplacement
    {

        solver          GAMG;
        tolerance       1e-8;
        relTol          0;
        smoother        GaussSeidel;
        cacheAgglomeration true;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }

    cellDisplacementFinal
    {

        solver          GAMG;
        tolerance       1e-10;
        relTol          0;
        smoother        GaussSeidel;
        cacheAgglomeration true;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;

    }
}

flowSolver
{
    solver            GMRES;
    GMRES
    {
        inviscidJacobian  LaxFriedrich;
        preconditioner    LUSGS;
        maxIter           3;
        nKrylov           5;
        solverTolRel      1e-1 (1e-1 1e-1 1e-1) 1e-1;
    }
}


pseudoTime
{
    nPseudoCorr       5; //40;
    nPseudoCorrMin    5; //40;
    pseudoTol         1e-3 (1e-3 1e-3 1e-3) 1e-3;
    pseudoCoNum       1e6; //4000
    pseudoCoNumMin    1e6; //4000
    pseudoCoNumMax    1e6; //4000
    localTimestepping true;
}


// ************************************************************************* //
