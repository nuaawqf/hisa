/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application       hisa;

startFrom         latestTime;

startTime         0;

stopAt            endTime;

endTime           150;

deltaT            1;

writeControl      timeStep;

writeInterval     1;

purgeWrite        2;

writeFormat       ascii;

writePrecision    6;

writeCompression  off;

timeFormat        general;

timePrecision     6;

runTimeModifiable true;

functions
{
    forces
    {
        type            forceCoeffs;
        libs ( "libforces.so" );
        writeControl   timeStep;
        writeInterval  1;

        patches
        (
            wall_4
        );

        log         true;
        rhoInf      1;
        rho         rhoInf;
        CofR        (0 0 0);
        liftDir     (-0.239733 0.970839 0);
        dragDir     (0.970839 0.239733 0);
        pitchAxis   (0 0 1);
        magUInf     618.022;
        lRef        1;
        Aref        1;
    }
}

// ************************************************************************* //
